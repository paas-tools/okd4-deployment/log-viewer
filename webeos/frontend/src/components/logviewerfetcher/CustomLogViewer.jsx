// https://www.thecodebuzz.com/react-fetch-http-post-request-state-hooks-examples/
import React, { useState, useEffect, useRef } from 'react';
import "@patternfly/react-core/dist/styles/base.css";
import { LogViewer, LogViewerSearch } from '@patternfly/react-log-viewer';
import {
   Select,
   SelectOption,
   Toolbar,
   ToolbarGroup,
   ToolbarContent,
   ToolbarToggleGroup,
   ToolbarItem,
   Divider,
   Tooltip,
   Button,
   Banner,
   DatePicker,
   TimePicker,
   Switch } from '@patternfly/react-core';
import DownloadIcon from '@patternfly/react-icons/dist/esm/icons/download-icon';
import FilterIcon from '@patternfly/react-icons/dist/esm/icons/filter-icon';

import utils from "../../utils";

export default function CustomLogViewer() {

  // Query params
  const search = window.location.search;
  const params = new URLSearchParams(search);
  const project = params.get('project');
  const userprovideddirectory = params.get('userprovideddirectory');

  // Params
  const [day,setDay] = useState(new Date().toISOString().split('T')[0]);
    
  // Obtain just the hour and the minutes (not the seconds)
  // Date are in ISOString (UTC)
  // StartTime is 15 mins before that EndTime (now)
  // Basically, by default will retrieve logs from the last 15 mins from now.
  const options = useState({
    hour12: false
  });
  const [startTime,setStartTime] = useState(new Date(new Date().getTime() - 15*60000).toISOString().split('T')[1]);
  const [endTime,setEndTime] = useState(new Date(new Date().getTime()).toISOString().split('T')[1]);
  const [endTimeTimePickerIsDisabled,setEndTimeTimePickerIsDisabled] = useState(false);

  const [isStreaming,setIsStreaming] = useState(true);

  const[action,setAction] = useState({
    isOpen: false,
    selected: "getlogaccess"
  });
  const actionOptions = [
      <SelectOption key={"key_access"} value="getlogaccess">Access</SelectOption>,
      <SelectOption key={"key_error"} value="getlogerror" >Error</SelectOption>
  ];
  
  const [size,setSize] = useState({
    isOpen: false,
    selected: "500"
  });
  const sizeOptions = [
    <SelectOption key={"key_500"} value="500" />,
    <SelectOption key={"key_1000"} value="1000" />,
    <SelectOption key={"key_2000"} value="2000" />,
    <SelectOption key={"key_2500"} value="2500" />,
    <SelectOption key={"key_5000"} value="5000" />,
    <SelectOption key={"key_max"} value="9999">Max</SelectOption>
  ];
  
  // Response
  // https://dev.to/shaedrizwan/building-custom-hooks-in-react-to-fetch-data-4ig6
  const [url] = useState(process.env.REACT_APP_BACKEND_ENDPOINT);
    
  const logViewerRef = React.useRef();
  const FooterButton = () => {
    const handleClick = e => {
      logViewerRef.current.scrollToBottom();
    };
    return <Button onClick={handleClick}>Jump to the bottom</Button>;
  };
 
  // Cannot export this
  // Check this out
  const [loading, setLoading] = useState(false);
  const [error,setError] = useState();
  const [data,setData] = useState([]);
    
  const timerIdRef = useRef(null);
  useEffect(() => {
    async function fetchData() {
      const requestOptions = {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(
          {
            action: action.selected,
            project: project,          
            userprovideddirectory: userprovideddirectory,          
            day: day,
            startTime: startTime,
            endTime: endTime,
            size: size.selected
          })
      };

      fetch(url, requestOptions)
      .then(response => response.json())
      .then(data => setData(utils.buildData(data.message,data.status)))
      .catch(setError)
      .finally(() => setLoading(false));
    }
        
    setEndTimeTimePickerIsDisabled(false);
    const TIMER = 5000; //auto-fetch every 5 seconds
    if (isStreaming){
      setEndTimeTimePickerIsDisabled(true);
      timerIdRef.current = setInterval(() => {
        // Only update endTime ('To') for streaming logs.
        setEndTime(new Date(new Date().getTime()).toISOString().split('T')[1]);
        // scroll log viewer to the end
        if((logViewerRef !== undefined) && (logViewerRef.current !== undefined)){
          logViewerRef.current.scrollToBottom();
        }
      }, TIMER);
    }else{
      setEndTimeTimePickerIsDisabled(false);
    }

    // Always call fetchData, even the first time to pre-load logs.    
    fetchData();
    
    return () => clearInterval(timerIdRef.current);
  // https://www.benmvp.com/blog/object-array-dependencies-react-useEffect-hook/
  // ffi: https://www.benmvp.com/blog/object-array-dependencies-react-useEffect-hook/#option-1---depend-on-the-pieces
  }, [url,day,startTime,endTime,action.selected,size.selected,isStreaming,project,userprovideddirectory]);
  
  return (
    <React.Fragment>
      <Toolbar>
        <ToolbarContent>
          <ToolbarToggleGroup toggleIcon={<FilterIcon />} breakpoint="lg">
          <ToolbarGroup variant="button-group">
            <ToolbarItem variant="label" id="stacked-date-select">
              Date:
            </ToolbarItem>
            <ToolbarItem>
              <Tooltip
                content={<div>In order to retrieve logs, first select a <b>Date</b>. By default, Today's date has been pre-selected.</div>}>
                <DatePicker
                  onChange={(str, date) => { setDay(str); } }
                  value={day}
                  aria-labelledby="stacked-date-select"                    
                />
              </Tooltip>
            </ToolbarItem>

            <ToolbarItem variant="label" id="stacked-starttime-select">
              From:
            </ToolbarItem>
            <ToolbarItem>
              <Tooltip
                content={<div>Select a time where the Log Viewer will start fetching logs from.</div>}>
                <TimePicker
                  onChange={(time,isValid) => {if(isValid){setStartTime(new Date(day + 'T' + time).toISOString().split('T')[1])}}}
                  time={new Date(day + 'T' + startTime).toLocaleTimeString('fr-FR',options)}
                  is24Hour
                  aria-labelledby="stacked-starttime-select"
                />
              </Tooltip>
            </ToolbarItem>

            <ToolbarItem variant="label" id="stacked-endtime-select">
                To:
            </ToolbarItem>
            <ToolbarItem>
              <Tooltip
                content={<div>Select a time where the Log Viewer will end fetching logs to. Note that this date is not editable but auto-updated when <b>Streaming</b> is enabled.</div>}>
                <TimePicker
                  // On change, set the UTC value instead
                  onChange={(time,isValid) => {if(isValid){setEndTime(new Date(day + 'T' + time).toISOString().split('T')[1])}}}
                  // Print time in Locale (i.e., no UTC)
                  time={new Date(day + 'T' + endTime).toLocaleTimeString('fr-FR',options)}
                  is24Hour
                  isDisabled={endTimeTimePickerIsDisabled}
                  aria-labelledby="stacked-endtime-select"
                />
              </Tooltip>
            </ToolbarItem>
            <ToolbarItem variant="label" id="stacked-streaming-select">
              Streaming:
            </ToolbarItem>
            <ToolbarItem>
              <Tooltip
                  content={<div>When <b>Streaming</b> is enabled, the Log Viewer will automatically stream logs from the time set in <b>From</b> until the time set in <b>To</b> every 5 seconds. Note that the <b>To</b> time is reset to the current time.</div>}>
                <Switch
                  id="reversed-switch"
                  isChecked={isStreaming}
                  onChange={() => setIsStreaming(!isStreaming)}
                  isReversed
                  aria-label="stacked-streaming-select"
                />
              </Tooltip>
            </ToolbarItem>
          </ToolbarGroup>

          <ToolbarItem variant="separator"></ToolbarItem>

          <ToolbarGroup>
            <ToolbarItem variant="label" id="stacked-action-select">
              Type:
            </ToolbarItem>              
            <ToolbarItem>
              <Tooltip
                content={<div>Type of logs that will be fetched.</div>}>
              <Select
                onToggle={() => { setAction({isOpen: !action.isOpen,selected: action.selected}) }}
                isOpen={action.isOpen}
                selections={action.selected}
                onSelect={(event, selection, isPlaceholder) => { setAction({isOpen: false, selected: selection})}}
                aria-labelledby="stacked-action-select"
                >{actionOptions}</Select>
              </Tooltip>
            </ToolbarItem>
            <ToolbarItem variant="label" id="stacked-size-select">
              Lines:
            </ToolbarItem>
            <ToolbarItem>
              <Tooltip
                content={<div><b>Lines</b> allows you to configure the maximum amount of hits (1 line = 1 hit) to be returned.</div>}>
              <Select
                onToggle={() => { setSize({isOpen: !size.isOpen,selected: size.selected}) }}
                isOpen={size.isOpen}
                selections={size.selected}
                onSelect={(event, selection, isPlaceholder) => { setSize({isOpen: false, selected: selection})}}
                aria-labelledby="stacked-size-select"
                >{sizeOptions}</Select>
              </Tooltip>
            </ToolbarItem>
          </ToolbarGroup>
          </ToolbarToggleGroup>              
        </ToolbarContent>
        <Divider />
      </Toolbar>

      <LogViewer
        ref={logViewerRef}  
        hasLineNumbers={true}
        height={800}
        data={data}
        theme={'dark'}
        toolbar={
          <Toolbar>
            <ToolbarContent>
              <ToolbarGroup variant="button-group">  
                <ToolbarItem variant="label" id="stacked-seach-select" alignment={"alignLeft"}>
                  Find: 
                </ToolbarItem>
                <ToolbarItem>
                  <Tooltip
                      content={<div>Search for something that is contained in the rendered logs.</div>}>
                    <LogViewerSearch placeholder="Search some word" aria-labelledby="stacked-seach-select"/>
                  </Tooltip>
                </ToolbarItem>

                <ToolbarItem alignment={"alignRight"}>
                  <Tooltip
                      content={<div>Download logs currently rendered in the Log Viewer panel to a txt file.</div>}>
                    <Button 
                      variant="link"
                      icon={<DownloadIcon />}
                      onClick={() => utils.downloadLogs(data)}>
                        Download logs
                    </Button>{' '}
                  </Tooltip>
                </ToolbarItem>
              </ToolbarGroup>
            </ToolbarContent>
          </Toolbar>
        }
        header={<Banner>{data.length} lines</Banner>}
        footer={<FooterButton />}
      />      
    </React.Fragment>
  );
}
